import re
import os

from subprocess import run

import pandas as pd

SUFF_TYPES = ("gcmask", "svmask", "lcmask")

p_repeats = re.compile('.*(Satellite|Simple_repeat|Low_complexity).*')
p_repeats_all = re.compile('.*(Satellite|Simple_repeat|Line|SINE|LTR).*')


def get_rootpath():
    sourcedir = os.path.dirname(os.path.abspath(workflow.snakefile))
    return sourcedir


def get_conda_perl5lib():
    # only the root of the perl lib directory is necessary
    # prevents from messing around with another perl installation
    return os.path.join(os.environ["CONDA_PREFIX"], "lib")


def check_and_set_params(params):
    root_path = get_rootpath()
    params['ROOT_DIR'] = root_path
    sv_dir = genomestrip_dir()
    classpath = sv_dir + "/lib/SVToolkit.jar"
    classpath += ":" + sv_dir + "/lib/gatk/GenomeAnalysisTK.jar"
    params['SV_DIR'] = sv_dir
    params['CLASSPATH'] = classpath
    params['GS_BWA_DIR'] = os.path.join(root_path, "bin", "bwa")

    return params


def genomestrip_dir():
    "Get SV_DIR from the genomestrip exectable"
    "resolve source until the file is no longer a symlink"
    source = which("genomestrip")
    while os.path.islink(source):
        pointsto = os.readlink(source)
        sourcedir = os.path.dirname(source)
        source = os.path.abspath(os.path.join(sourcedir, pointsto))
    return os.path.dirname(source)


def svmask_index(infasta, outfasta):
    # TODO provide config as an argument
    gstrip_bwa = os.path.join(config['GS_BWA_DIR'], "bwa")
    infasta_abs = os.path.abspath(infasta)
    os.symlink(infasta_abs, outfasta)
    command = "samtools faidx %s" % outfasta
    run(command, shell=True, check=True)
    genome_size = get_genome_size(infasta)
    if genome_size <= 10000000:
        bwa_index_param = "-a is"
    else:
        bwa_index_param = "-a bwtsw"
    command = "%s index %s %s" % (gstrip_bwa, bwa_index_param, outfasta)
    run(command, shell=True, check=True)


def get_genome_size(genome):
    """ Compute genome size from genome faidx file """
    sequences = pd.read_csv(genome+".fai", sep="\t", header=None)
    genome_size = sum(sequences[1])
    return genome_size


def set_absolute_path(params, key):
    params["_"+key] = params[key]
    params[key] = os.path.abspath(params["_"+key])
