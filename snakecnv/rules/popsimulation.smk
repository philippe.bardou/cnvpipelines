import os

checkpoint get_fasta_popsim:
    input:
        fasta = "reference_raw.fasta",
        index = "reference_raw.fasta.fai"
    output:
        "reference.fasta",
        "reference.fasta.fai"
    params:
        chromosomes = config['chromosomes']
    shell:
        "samtools faidx {input.fasta} {params.chromosomes} > {output}; "
        "samtools faidx {output}"


rule buildpop:
    input:
        reference = cmd.get_reference,
        nstretches = "exclusion/gaps.bed"
    output:
        cmd.get_builpop_fastqs(),
        cmd.get_builpop_fastas(),
        genotype = "pop/genotypes.vcf.gz",
        yamlconf = temp("builpop_params.yaml"),
    params:
        outdir = "pop"
    threads:
        get_threads("builpop", 8)
    log:
        stdout = "logs/popsim.o",
        stderr = "logs/popsim.e"
    run:
        import yaml
        with open(output.yamlconf, 'w') as outfile:
            yaml.dump(config, outfile, default_flow_style=False)
        command = ["buildpop_launcher.py",
                   "--reference %s" % input.reference,
                   "--conf-file %s" % output.yamlconf,
                   "--nstretches %s" % input.nstretches,
                   "--outdir %s" % params.outdir,
                   "--threads %s" % threads]
        shell(" ".join(command) + " 1> %s 2> %s" % (log.stdout, log.stderr))


rule linkdatabams:
    input:
        bam = "bams/{sample}.bam",
        bai = "bams/{sample}.bam.bai"
    output:
        bam = "data/bams/{sample}.bam",
        bai = "data/bams/{sample}.bam.bai"
    run:
        os.symlink(os.path.abspath(input.bam), output.bam)
        os.symlink(os.path.abspath(input.bai), output.bai)


rule buildinfilesresults:
    input:
        unpack(cmd.buildinfilesresults)
    output:
        genotypes = "list_files/tools_results_files_{svtype}.list",
        filtered = "list_files/filtered_results_files_{svtype}.list"
    threads:
        1
    run:
        with open(output.genotypes, "w") as o_gt:
            for i_gt in input.genotypes:
                o_gt.write("%s\n" % i_gt)
        with open(output.filtered, "w") as o_ft:
            for i_ft in input.filtered:
                o_ft.write("%s\n" % i_ft)

rule buildresults:
    input:
        genotypes = "list_files/tools_results_files_{svtype}.list",
        filtered = "list_files/filtered_results_files_{svtype}.list",
        true_vcf = "pop/genotypes.vcf.gz"
    output:
        "results/{svtype}/Summarized_results_{svtype}.ipynb",
        "results/{svtype}/Summarized_results_{svtype}.html"
    threads:
        1
    log:
        stdout = "logs/results_{svtype}.o",
        stderr = "logs/results_{svtype}.e"
    shell:
        """
            touch results/{wildcards.svtype}/Summarized_results_{wildcards.svtype}.ipynb
            touch results/{wildcards.svtype}/Summarized_results_{wildcards.svtype}.html
        """
