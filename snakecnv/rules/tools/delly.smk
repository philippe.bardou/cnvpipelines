delly_sv = ['DEL', 'INV', 'DUP']

rule delly:
    input:
        bamlist = "{batch}/bamlist.list",
        excluded = "exclusion/excluded.bed",
        reference = cmd.get_reference,
        fai = "%s.fai" % cmd.get_reference
    output:
        "{batch}/delly/delly_{chrom}_{svtype}.bcf"
    params:
        template = cmd.get_template,
    threads:
        get_threads("delly", 6)
    log:
        stdout = "{batch}/logs/delly/{chrom}_{svtype}.o",
        stderr = "{batch}/logs/delly/{chrom}_{svtype}.e"
    shell:
        "export  OMP_NUM_THREADS={threads} ; "
        "delly.py -b {input.bamlist} -c {wildcards.chrom}"
        " -g {input.reference} -x {input.excluded} -t {wildcards.svtype}"
        " -o {output} -e {params.template}"
        " 1>{log.stdout} 2>{log.stderr}"
