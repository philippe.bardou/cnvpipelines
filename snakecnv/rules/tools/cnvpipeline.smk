import os

cnvpipeline_sv = ['mCNV']


def getChromLength(reference, chrom):
    """
    Retrieve the length of chromosome chrom from .fai file
    :param reference: the genome fasta file
    :param chrom: the chromosome of interest
    :type reference: file
    :type chrom: str
    :return: chromosome length
    :rtype: int
    """
    reference_fai = str(reference) + ".fai"
    chroms = {}
    if reference is not None and os.path.isfile(reference_fai):
        with open(reference_fai) as fai_file:
            for line in fai_file:
                line_items = line.strip().split("\t")
                name, length = line_items[0:2]
                name = name.split(" ")[0]
                chroms[name] = int(length)
    if chrom not in chroms:
        raise KeyError('the chromosome '+chrom+' is not in the *.fai file....')
    return chroms[chrom]



def intervallist(reference, chrom, output):
    chrom_length = getChromLength(reference, chrom)
    chrlistFh = open(output, "w")
    chrlistFh.write(chrom + ":" + str(1) + "-" + str(chrom_length) + "\n")
    chrlistFh.close()


rule intervallist:
    input:
        reference = cmd.get_reference
    output:
        intervallist = "cnvpipeline/{chrom}/interval.list"
    run:
        intervallist(input.reference, wildcards.chrom, output.intervallist)

rule cnvpipeline:
    input:
        unpack(cmd.get_input_bams),
        bamlist = "{batch}/bamlist.list",
        gendermap = "{batch}/genomestrip/gendermap.txt",
        metadatadone = "{batch}/genomestrip/metadata/sample_gender.report.txt",
        intervallist = "cnvpipeline/{chrom}/interval.list",
        genome = config['refbundle'] if 'refbundle' in config else  cmd.get_reference
    params:
        metadata = "{batch}/genomestrip/metadata"
    output:
        "{batch}/cnvpipeline/{chrom}/cnvpipeline_{chrom}.vcf.gz"
    log:
        stdout="{batch}/logs/cnvpipeline/{chrom}.o",
        stderr="{batch}/logs/cnvpipeline/{chrom}.e"
    shell:
        "cnvpipeline.py --bamlist {input.bamlist} --reference {input.genome} --metadata {params.metadata} "
        "--gendermap {input.gendermap} --outdir {wildcards.batch}/cnvpipeline/{wildcards.chrom} "
        "--outprefix cnvpipeline_{wildcards.chrom} --interval-list {input.intervallist} 1> {log.stdout} 2> {log.stderr}"
