genomestrip_sv = ['DEL']

rule gendermap:
    input:
        unpack(cmd.get_input_bams)
    output:
        gendermap = "{batch}/genomestrip/gendermap.txt"
    run:
        fout = open(output.gendermap, "w")
        for sample in cmd.get_batch_samples(wildcards.batch):
            fout.write("\t".join([sample, "F"])+"\n")
        fout.close()

if "genomestrip" in cmd.get_tools:

    PREFIX = os.path.splitext(config["refbundle"])[0]

    rule preprocess:
        input:
            PREFIX+".fasta.fai",
            PREFIX+".gcmask.fasta.fai",
            PREFIX+".svmask.fasta.fai",
            PREFIX+".lcmask.fasta.fai",
            PREFIX+".rdmask.bed",
            PREFIX+".dict",
            PREFIX+".ploidymap.txt",
            PREFIX+".gendermask.bed",
            # PREFIX+".Nstretch.bed",
            unpack(cmd.get_input_bams),
            bamlist = "{batch}/bamlist.list",
            gendermap = "{batch}/genomestrip/gendermap.txt",
            genome = config['refbundle']
        output:
            metadata = "{batch}/genomestrip/metadata/sample_gender.report.txt"
        log:
            stdout = "{batch}/logs/genomestrip/preprocess.o",
            stderr = "{batch}/logs/genomestrip/preprocess.e"
        shell:
            "gstrip_preprocess.py --bamlist {input.bamlist} --reference {input.genome} --gendermap {input.gendermap} "
            "--outdir {wildcards.batch}/genomestrip --outprefix preprocess "
            "1>{log.stdout} 2>{log.stderr}"

rule genomestrip:
    input:
        unpack(cmd.get_input_bams),
        bamlist = "{batch}/bamlist.list",
        gendermap = "{batch}/genomestrip/gendermap.txt",
        metadatadone = "{batch}/genomestrip/metadata/sample_gender.report.txt",
        genome = config['refbundle'] if 'refbundle' in config else cmd.get_reference
    params:
        metadata = "{batch}/genomestrip/metadata"
    output:
        "{batch}/genomestrip/genomestrip_{chrom}.vcf.gz"
    log:
        stdout = "{batch}/logs/genomestrip/{chrom}.o",
        stderr = "{batch}/logs/genomestrip/{chrom}.e"
    shell:
        "genomestrip.py --bamlist {input.bamlist} --chrom {wildcards.chrom} --reference {input.genome} "
        "--metadata {params.metadata} --gendermap {input.gendermap} --outdir {wildcards.batch}/genomestrip "
        "--outprefix genomestrip_{wildcards.chrom} 1>{log.stdout} 2>{log.stderr}"
