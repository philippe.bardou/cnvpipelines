import sys
import re
import os

rule bwaindex:
    input:
        cmd.get_reference
    output:
        "%s.bwt" % cmd.get_reference
    log:
        stdout = "logs/index.o",
        stderr = "logs/index.e"
    shell:
        "bwa index {input} 1> {log.stdout} 2> {log.stderr}"

rule bwamap:
    input:
        unpack(cmd.get_sample_fastq),
        reference = cmd.get_reference,
        ref_index = "%s.bwt" % cmd.get_reference
    output:
        bam = temp("bwa/{sample}.bam")
    params:
        rg = cmd.get_read_group
    threads:
        get_threads("bwamap", 8)
    log:
        stdout = "logs/map_{sample}.o",
        stderr = "logs/map_{sample}.e"
    shell:
        "rm -f {output.bam}*; "
        "bwa mem -t {threads} -M -R {params.rg} "
        "{input.reference} {input.fastq} 2> {log.stderr} | "
        "samtools view -bS - 2> {log.stderr} | "
        "samtools sort -o {output.bam} 1> {log.stdout} 2> {log.stderr}"

rule bamindex:
    input:
        "{folder}/{sample}.bam"
    output:
        bai = "{folder}/{sample}.bam.bai",
        summary = "{folder}/{sample}.bam.summary"
    wildcard_constraints:
        folder = "(bams|bwa)"
    threads:
        1
    log:
        stdout = "logs/mapindex_{sample}_{folder}.o",
        stderr = "logs/mapindex_{sample}_{folder}.e"
    shell:
        "samtools index {input}; "
        "samtools flagstat {input} > {output.summary}"

rule markduplicate:
    input:
        bam = "bwa/{sample}.bam",
        bai = "bwa/{sample}.bam.bai"
    output:
        bam = "bams/{sample}.bam",
        metrics = "bams/{sample}.bam.metrics"
    threads:
        1
    log:
        stdout = "logs/markduplicates_{sample}.o",
        stderr = "logs/markduplicates_{sample}.e"
    shell:
        "picard MarkDuplicates I={input.bam} O={output.bam} M={output.metrics} "
        " REMOVE_DUPLICATES=true 1> {log.stdout} 2> {log.stderr} "
