#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pysam import Fastafile, tabix_index

from svrunner_utils import eprint, get_contigs, sorting

from svreader.genomestrip import GenomeSTRIPReader, GenomeSTRIPWriter
from svreader.delly import DellyReader, DellyWriter
from svreader.lumpy import LumpyReader, LumpyWriter
from svreader.pindel import PindelReader, PindelWriter

tool_to_reader = {"pindel": PindelReader, "delly": DellyReader,
                  "lumpy": LumpyReader,  "genomestrip": GenomeSTRIPReader}
tool_to_writer = {"pindel": PindelWriter, "delly": DellyWriter,
                  "lumpy": LumpyWriter, "genomestrip": GenomeSTRIPWriter}


def convert_svtool_to_vcf(reference, inputfile, gaps, outputfile,
                          toolname, svtype,
                          minlen=50, maxlen=5000000,
                          batch=None, doNotFilter=False):

    # The reference handle
    reference_handle = Fastafile(reference) if reference else None
    reference_contigs = get_contigs(reference)

    SVReader = tool_to_reader[toolname](inputfile,
                                        reference_handle=reference_handle,
                                        svs_to_report=[svtype])
    SVWriter = tool_to_writer[toolname](outputfile,
                                        reference_contigs,
                                        SVReader)

    # The records
    eprint("Now reading the records")
    records = []
    for record in SVReader:
        records.append(record)
    eprint("%d records" % (len(records)))

    # Merging records (necessary for delly inversions)
    records = SVReader.bnd_merge(svtype, records)

    # Now filtering the records
    if not doNotFilter:
        eprint("Now filtering")
        records = SVReader.filtering(records, minlen, maxlen, gaps)

    # Removing duplicated entries (only performed for pindel records)
    eprint("Removing duplicates")
    records = SVReader.remove_duplicate(records)
    eprint("%d records" % (len(records)))

    # Sorting the vcf records
    records = sorting(records, reference_contigs)

    eprint("Now writing the records")
    for record in records:
        record.addbatch2Id(batch)
        SVWriter.write(record)
    SVWriter.close()
    eprint("%d records" % (len(records)))


def parsing(reference, tooloutput, gaps, parseoutput, tool, svtype,
            doNotFilter, batch):
    #print("\n".join([reference, tooloutput, gaps, parseoutput, tool, svtype]))

    convert_svtool_to_vcf(reference, tooloutput, gaps, parseoutput,
                          tool, svtype, batch=batch, doNotFilter=doNotFilter)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="parse.py",
        description="Parse tool output into a valid VCF file")
    parser.add_argument("-r", "--reference", required=True,
                        help="Reference fasta file")
    parser.add_argument("-i", "--tool-output", required=True,
                        help="Tool output file")
    parser.add_argument("-g", "--gaps", required=True,
                        help="Gaps bed file")
    parser.add_argument("-o", "--output-vcf", required=True,
                        help="Output vcf file")
    parser.add_argument("-t", "--tool", required=True,
                        help="Tool name")
    parser.add_argument("-s", "--sv-type", required=True,
                        help="Type of variant")
    parser.add_argument("-b", "--batch", default=None,
                        help="the batch")
    parser.add_argument("-f", "--nofilter", action="store_true", default=False,
                        help="Filter false : do not filter during parse")

    args = parser.parse_args()

    parsing(reference=args.reference,
            tooloutput=args.tool_output,
            gaps=args.gaps,
            parseoutput=args.output_vcf,
            tool=args.tool,
            svtype=args.sv_type,
            batch=args.batch,
            doNotFilter=args.nofilter
            )
