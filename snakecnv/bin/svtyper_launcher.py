#!/usr/bin/env python

from __future__ import print_function

import os
import shutil
import traceback
import sys
import tempfile
from subprocess import call
from pysam import VariantFile, tabix_index
from svtyper_utils import genotype_multiple_samples


def input_is_empty(input_vcf):
    is_empty = True
    vcf = VariantFile(input_vcf)

    # Check if is empty:
    for record in vcf:
        is_empty = False
        break

    return is_empty


def launch(bamlist, input_vcf, output_vcf, threads):
    try:
        if input_is_empty(input_vcf):
            shutil.copy(input_vcf, output_vcf)
            tbi = input_vcf + ".tbi"
            if os.path.isfile(tbi):
                shutil.copy(tbi, output_vcf + ".tbi")
            else:
                tabix_index(output_vcf)
        else:
            vcf_out = output_vcf[:-3]
            genotype_multiple_samples(bamlist=bamlist,
                                      vcf_in=input_vcf,
                                      vcf_out=vcf_out,
                                      cores=threads)
            tmpdir = tempfile.mkdtemp()
            command = "bcftools sort -O z -T {tmpdir} ". format(tmpdir=tmpdir)
            command += "-o {ovcf} {ivcf} ".format(ivcf=vcf_out, ovcf=output_vcf)
            returncode = call(command, shell=True)
            if returncode == 0:
                os.remove(vcf_out)
                tabix_index(output_vcf, force=True, preset="vcf")
            else:
                print("Bctools sort command failed", file=sys.stderr)
                return 1
    except:
        # Except all exception
        traceback.print_exc()
        return 1
    return 0


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="svtyper_launcher.py",
        description="Launch svtyper on data")
    parser.add_argument("-B", "--bamlist", required=True, type=str,
                        help="File listing all input bam files")
    parser.add_argument("-i", "--input-vcf", required=True, type=str,
                        help="Input VCF file")
    parser.add_argument("-o", "--output-vcf", required=True, type=str,
                        help="Output VCF file")
    parser.add_argument("-t", "--threads", required=True, type=int,
                        help="Number of threads to use")

    args = parser.parse_args()

    exit(launch(**{k: v for k, v in vars(args).items() if k in launch.__code__.co_varnames
           or ("kwargs" in launch.__code__.co_varnames and k != "func")}))
