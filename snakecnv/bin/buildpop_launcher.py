#!/usr/bin/env python

import sys
import os
import configparser
import traceback
from subprocess import call
import yaml


# PATCH convert yaml config file to conf file in INI format for build_variant_pop
def reformat_configyaml(yaml_config, ini_conf_file):
    yaml_config = read_yaml(yaml_config)
    config = configparser.ConfigParser()
    config["Defaults"] = yaml_config
    with open(ini_conf_file, 'w') as configfile:    # save
        config.write(configfile)


def read_yaml(yaml_config_file):
    with open(yaml_config_file, 'r') as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            exit(1)
    return config


def launch(reference, conf_file, outdir, threads, nstretches=None):

    # Convert yaml config file to ini file
    ini_conf_file = "conffile.ini"
    reformat_configyaml(conf_file, ini_conf_file)

    command = "build_variant_pop.py "
    command += "--reference {ref} --conf-file {cfile} -o {odir} "\
               "-t {threads} -e -q ".format(ref=reference,
                                           cfile=ini_conf_file,
                                           odir=outdir,
                                           threads=threads)
    if nstretches is not None:
        command += "--nstretches %s" % nstretches

    returncode = call(command, shell=True)
    if returncode == 0:
        os.remove(ini_conf_file)
    else:
        print("build_variant_pop.py command failed", file=sys.stderr)
        return 1
    return 0


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="buildpop_launcher.py",
        description="Launch builpop simulation")

    parser.add_argument("-r", "--reference", required=True,
                        help="refernece genome")
    parser.add_argument("--conf-file", required=True,
                        help="config file, yaml forma",
                        metavar="FILE")
    parser.add_argument("-ns", "--nstretches",
                        help="N-stretches positions bed file",
                        required=False)
    parser.add_argument("-o", "--outdir", required=True,
                        help="Specify the output dir")
    parser.add_argument("-t", "--threads",  type=int,
                        help="Number of threads to use", default=1)

    args = parser.parse_args()

    exit(launch(**{k: v for k, v in vars(args).items() if k in launch.__code__.co_varnames
           or ("kwargs" in launch.__code__.co_varnames and k != "func")}))
