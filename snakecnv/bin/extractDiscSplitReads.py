#!/usr/bin/env python

from __future__ import print_function

import sys
import os
import argparse
import re

import multiprocessing
from functools import partial

import pysam

from svrunner_utils import fetchId

CODE2CIGAR = "MIDNSHP=XB"
CIGAR2CODE = dict([y, x] for x, y in enumerate(CODE2CIGAR))
CIGAR2CODE = dict([y, x] for x, y in enumerate(CODE2CIGAR))

BAM_CMATCH = 0  # M
BAM_CINS = 1  # I
BAM_CDEL = 2  # D
BAM_CREF_SKIP = 3  # N
BAM_CSOFT_CLIP = 4  # S
BAM_CHARD_CLIP = 5  # H
BAM_CPAD = 6  # P
BAM_CEQUAL = 7  # =
BAM_CDIFF = 8  # X
BAM_CBACK = 9  # 9
NM = 10  # NM

CIGAR_REGEX = re.compile("(\d+)([MIDNSHP=XB])")


def eprint(*args, **kwargs):
    ''' A simple to sys.stderr printer wrapper '''
    print(*args, file=sys.stderr, **kwargs)


def cigartuples(cigar):
    if cigar is None or len(cigar) == 0:
        cigartuples = []
    else:
        parts = CIGAR_REGEX.findall(cigar)
    # reverse order
    cigartuples = [(CIGAR2CODE[y], int(x)) for x, y in parts]
    return cigartuples


def get_bamfiles(bamlist):
    with open(bamlist, "r") as fin:
        bamfiles = [f.strip() for f in fin]
    return bamfiles


class cigarOp (object):

    """
    struct to store a discrete CIGAR operations
    """

    def __init__(self, opLength, op):
        self.length = int(opLength)
        self.op = op

    def __str__(self):
        return str(self.length) + self.op

    def __repr__(self):
        return self.__str__()


class queryPos (object):

    """
    struct to store the start and end positions of query CIGAR operations
    """

    def __init__(self, qsPos, qePos, qLen):
        self.qsPos = int(qsPos)
        self.qePos = int(qePos)
        self.qLen = int(qLen)

    def __str__(self):
        return "[%d:%d] %d" % (self.qsPos, self.qePos, self.qLen)


def calcQueryOverlap(s1, e1, s2, e2):
    o = 1 + min(e1, e2) - max(s1, s2)
    return max(0, o)


def extractDiscSplitReads(bamfiles, chrom, outdir=None,
                          numSplits=2, includeDups=False, minNonOverlap=20,
                          cores=1):
    if outdir is None or not os.path.exists(outdir):
        eprint("outdir should be a valid and existing dir")
        exit(1)
    pool = multiprocessing.Pool(processes=cores)
    launch_ind = partial(extractDiscSplitsFromBwaMem,
                         chrom=chrom,
                         outdir=outdir,
                         numSplits=numSplits,
                         includeDups=includeDups,
                         minNonOverlap=minNonOverlap)
    results = pool.map(launch_ind, bamfiles)
    return results


def extractDiscSplitsFromBwaMem(inFile, chrom=None, outdir=".",
                                numSplits=2, includeDups=False,
                                minNonOverlap=20):
    """
    Extract simulteanoulsy discordant and split reads from a bam file
    """
    eprint("Processing file %s for chrom %s" % (inFile, chrom))
    sample_id = fetchId(inFile)
    splitters = os.path.join(outdir, sample_id + ".splitters.bam")
    discordants = os.path.join(outdir, sample_id + ".discordants.bam")
    samfile = pysam.AlignmentFile(inFile, "rb")
    splitreads = pysam.AlignmentFile(splitters, "wb", template=samfile)
    discreads = pysam.AlignmentFile(discordants, "wb", template=samfile)
    for f in samfile.fetch(chrom):
        if not includeDups and f.is_duplicate:
            continue
        if not f.flag & 1294:
            discreads.write(f)
        if not f.has_tag('SA'):
            continue
        tag = f.get_tag('SA')
        num_alt_align = len(tag.split(";"))
        if num_alt_align <= numSplits:
            mate = tag.split(",")
            mateCigar = mate[3]
            mate_is_reverse = False if mate[2] == "+" else True
            suffix = "_1" if f.is_read1 else "_2"
            f.query_name += suffix
            readQueryPos = calcQueryPosFromCigar(f.cigartuples,
                                                 f.is_reverse)
            mateQueryPos = calcQueryPosFromCigar(cigartuples(mateCigar),
                                                 mate_is_reverse)

            overlap = calcQueryOverlap(readQueryPos.qsPos,
                                       readQueryPos.qePos,
                                       mateQueryPos.qsPos,
                                       mateQueryPos.qePos)
            nonOverlap1 = 1 + readQueryPos.qePos - readQueryPos.qsPos - overlap
            nonOverlap2 = 1 + mateQueryPos.qePos - mateQueryPos.qsPos - overlap
            mno = min(nonOverlap1, nonOverlap2)
            if mno >= minNonOverlap:
                splitreads.write(f)
    splitreads.close()
    discreads.close()
    return sample_id, discordants, splitters


def calcQueryPosFromCigar(cigartuples, is_reverse):
    qsPos = 0
    qePos = 0
    qLen = 0
    if is_reverse:
        cigartuples.reverse()
    front = True
    for (op, length) in cigartuples:
        if front and (op == BAM_CHARD_CLIP
                      or op == BAM_CSOFT_CLIP):
            qsPos += length
            qePos += length
            qLen += length
        elif not front and (op == BAM_CHARD_CLIP
                            or op == BAM_CSOFT_CLIP):
            qLen += length
        elif (op == BAM_CMATCH
              or op == BAM_CINS):
            qePos += length
            qLen += length
            front = False
    return queryPos(qsPos, qePos, qLen)


def parse_arguments():
    parser = argparse.ArgumentParser(
        prog="extractDiscSplitReads_BwaMem",
        description=" Get split-read alignments from bwa-mem in lumpy "
                    " compatible format. Ignores reads marked as duplicates.")
    group_ind = parser.add_mutually_exclusive_group(required=True)
    group_ind.add_argument("-i", "--inFile", dest="inFile",
                           help="A BAM file")
    group_ind.add_argument("-l", "--bamlist", dest="bamlist",
                           help="file with a list of bam files")
    parser.add_argument(
           "-c", "--chrom",
           help="a chromosome")
    parser.add_argument(
           "-o", "--outdir",
           help="output directory for discordants and splitters")
    parser.add_argument(
           "-n", "--numSplits",
           default=2, type=int,
           help="The maximum number of split-read mappings to allow per read."
                 " Reads with more are excluded. Default=2")
    parser.add_argument(
            "-d", "--includeDups",
            action="store_true", default=0,
            help="Include alignments marked as duplicates. Default=False")
    parser.add_argument(
            "-m", "--minNonOverlap",
            default=20, type=int,
            help="minimum non-overlap between split alignments "
                 "on the query (default=20)")
    parser.add_argument(
            "-t", "--threads",
            default=1, type=int,
            help="number of threads")
    args = parser.parse_args()

    return args


def main():
    args = parse_arguments()

    if args.inFile is not None:
        bamfiles = [args.inFile]
    else:
        bamfiles = get_bamfiles(args.bamlist)

    extractDiscSplitReads(bamfiles, args.chrom,
                          outdir=args.outdir,
                          numsplits=args.numSplits,
                          includedups=args.includeDups,
                          minnonoverlap=args.minNonOverlap,
                          threads=args.threads)


if __name__ == "__main__":
    sys.exit(main())
