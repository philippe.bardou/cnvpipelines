#!/usr/bin/env python

from __future__ import print_function

import sys
import os
import argparse
from tempfile import mkdtemp
from pathlib import Path
from shutil import rmtree
from subprocess import call

from pysam import tabix_compress, tabix_index

from extractDiscSplitReads import extractDiscSplitReads
from insert_size_distro import insertsizes, NoReadsException


def eprint(*args, **kwargs):
    ''' A simple to sys.stderr printer wrapper '''
    print(*args, file=sys.stderr, **kwargs)


def existing_file_abs_path(f):
    try:
        Path(f).resolve()
    except FileNotFoundError:
        print("File %s is not an existing and valid file" % f)
        exit(1)
    return os.path.abspath(f)


def file_abs_path(f):
    return os.path.abspath(f)


def get_bamfiles(bamlist):
    with open(bamlist, "r") as fin:
        bamfiles = [existing_file_abs_path(f.strip()) for f in fin]
    return bamfiles


def get_sample_desc_files(discsplit_files, insert_sizes):
    samples = dict()
    for sample, disc, split in discsplit_files:
        samples[sample] = {'disc': disc, 'split': split}
    for sample, mean, stdev, histo_file, readlength in insert_sizes:
        if sample not in samples:
            eprint("sample %s absent from insert sizes" % sample)
            exit(1)
        samples[sample]['lumpy'] = {'id': sample,
                                    'mean': mean,
                                    'stdev': stdev,
                                    'read_length': readlength,
                                    'min_non_overlap': readlength,
                                    'histo_file': histo_file}
    return samples


def dict_str(k, v):
    return "%s:%s" % (k, v)


def disc_string(disc_file, sample_info,
                min_non_overlap=101, discordant_z=5,
                back_distance=10, weight=1, min_mapping_threshold=20):

    lumpy_dict = {'min_non_overlap': min_non_overlap,
                  'discordant_z': discordant_z,
                  'back_distance': back_distance,
                  'weight': weight,
                  'min_mapping_threshold': min_mapping_threshold}

    lumpy_dict['bam_file'] = disc_file
    lumpy_dict.update(sample_info)

    return ",".join([dict_str(k, v) for k, v in lumpy_dict.items()])


def split_string(split_file, sample_info,
                 back_distance=10, weight=1, min_mapping_threshold=20):

    lumpy_dict = {'back_distance': back_distance,
                  'weight': weight,
                  'min_mapping_threshold': min_mapping_threshold}

    lumpy_dict['bam_file'] = split_file
    lumpy_dict['id'] = sample_info['id']

    return ",".join([dict_str(k, v) for k, v in lumpy_dict.items()])

#    -pe id:sample,bam_file:sample.discordants.bam,histo_file:sample.lib1.histo,mean:500,stdev:50,read_length:101,min_non_overlap:101,discordant_z:5,back_distance:10,weight:1,min_mapping_threshold:20 \
#    -sr id:sample,bam_file:sample.splitters.bam,back_distance:10,weight:1,min_mapping_threshold:20 \


def runLumpy(args):
    bamlist = args.bamlist
    chrom = args.chrom
    output = file_abs_path(args.output)
    threads = args.threads
    verbose = args.verbose

    bamfiles = get_bamfiles(bamlist)

    tempdir = mkdtemp(dir=".")
    oldir = os.getcwd()
    os.chdir(tempdir)

    # Extract discordants and splitters
    discsplit_files = extractDiscSplitReads(bamfiles, chrom, outdir=".",
                                            cores=threads)

    # Extract insert size distribution
    try:
        insert_sizes = insertsizes(bamfiles, chrom, outdir=".", cores=threads)
    except NoReadsException as e:
        print(e)
        tabix_compress(args.template, output, force=True)
        tabix_index(output, force=True, preset="vcf")
        return 0

    samples = get_sample_desc_files(discsplit_files, insert_sizes)

    # construct lumpy string
    temp_output = "lumpy.vcf"
    lumpy_str = "lumpy -mw 4 -tt 0 "
    for sample, infos in samples.items():
        pe_str = disc_string(infos['disc'], infos['lumpy'])
        sr_str = split_string(infos['split'], infos['lumpy'])
        lumpy_str += " -pe " + pe_str + " -sr " + sr_str
    lumpy_str += " | vcf-sort > " + temp_output
    if verbose:
        eprint(lumpy_str)
    completed = call(lumpy_str, shell=True)

    tabix_compress(temp_output, output, force=True)
    tabix_index(output, force=True, preset="vcf")

    os.chdir(oldir)
    rmtree(tempdir)

    return completed


def parse_arguments():
    parser = argparse.ArgumentParser(
        prog="lumpy.py",
        description="Lumpy-sv wrapper for a specific chromosome  "
                    "  ")
    parser.add_argument("-b", "--bamlist", required=True,
                        help="A file with a list of BAM files")
    parser.add_argument("-c", "--chrom", required=True,
                        help="a chromosome")
    parser.add_argument("-o", "--output", required=True,
                        help="the output file")
    parser.add_argument("-e", "--excluded",
                        help="File describing regions to exclude")
    parser.add_argument("-t", "--threads",
                        default=1, type=int,
                        help="number of threads")
    parser.add_argument("-v", "--verbose", action="store_true", default=False,
                        help="increase verbosity")
    parser.add_argument("--template", required=True, help="Empty template file")

    args = parser.parse_args()

    return args


def main():
    args = parse_arguments()
    runLumpy(args)


if __name__ == "__main__":
    sys.exit(main())
