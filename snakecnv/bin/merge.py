#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Usage:
Snakemake script

"""

import os
import tempfile
import shutil
from subprocess import call

from svrunner_utils import addContigInfosTovcf


def concatenate(vcf_files, outputfile, reference):
        tempdir = tempfile.mkdtemp()
        vcfFileslist = tempdir + "/" + "vcffile.list"
        with open(vcfFileslist, "w") as outfh:
            for gfile in vcf_files:
                vcf_sample_dropped_file = tempdir + "/" + os.path.basename(gfile)
                cmd = "bcftools view --drop-genotypes " + gfile + \
                    " |  vcf-sort -c | bgzip -c >" + vcf_sample_dropped_file
                cmd += " && tabix -p vcf " + vcf_sample_dropped_file
                call(cmd, shell=True)
                outfh.write(vcf_sample_dropped_file + "\n")
        temp_outputfile = tempdir + "/" + "merge.vcf"
        cmd = "bcftools concat -f " + vcfFileslist + \
              " -a | vcf-sort -c  > " + temp_outputfile
        call(cmd, shell=True)

        # drop format info from header
        temp_outputfile_newheader = tempdir + "/" + "merge_newheader.vcf"
        cmd = "bcftools view -h " + temp_outputfile + \
            " | grep -v '^##FORMAT' > " + tempdir + "/newheader ;"
        cmd += " bcftools reheader -h " + tempdir + "/newheader " + \
            temp_outputfile + " > " + temp_outputfile_newheader
        call(cmd, shell=True)

        # Correct contig infos here using bcftools annotate
        addContigInfosTovcf(temp_outputfile_newheader, outputfile, reference)
        shutil.rmtree(tempdir)


def merging(inputfiles, outputfile, reference):
    print("\n".join(["\t".join(inputfiles), outputfile]))

    concatenate(inputfiles, outputfile, reference)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="merge.py",
        description="Merge VCF files into a single one")
    parser.add_argument("-i", "--input-vcfs", required=True,
                        help="Vcf input files", nargs='+')
    parser.add_argument("-o", "--output-vcf", required=True,
                        help="Merged vcf output file")
    parser.add_argument("-r", "--reference", required=True,
                        help="Reference file")

    args = parser.parse_args()

    merging(inputfiles=args.input_vcfs,
            outputfile=args.output_vcf,
            reference=args.reference)
