#!/usr/bin/env python3

import shutil
import sys


with open(sys.argv[1], 'wb') as wfp:
    for fn in sys.argv[2:]:
        with open(fn, 'rb') as rfp:
            shutil.copyfileobj(rfp, wfp)