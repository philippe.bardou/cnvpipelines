#!/usr/bin/env python
#  (c) 2012 - Ryan M. Layer
#  Hall Laboratory
#  Quinlan Laboratory
#  Department of Computer Science
#  Department of Biochemistry and Molecular Genetics
#  Department of Public Health Sciences and Center for Public Health Genomics,
#  University of Virginia
#  rl6sf@virginia.edu

from __future__ import print_function


import sys
import os
import argparse
from collections import Counter

import multiprocessing
from functools import partial

import numpy as np
import pysam

from svrunner_utils import fetchId

class NoReadsException(Exception):
    pass


def eprint(*args, **kwargs):
    ''' A simple to sys.stderr printer wrapper '''
    print(*args, file=sys.stderr, **kwargs)


def get_bamfiles(bamlist):
    with open(bamlist, "r") as fin:
        bamfiles = [f.strip() for f in fin]
    return bamfiles


def unscaled_upper_mad(xs):
    """Return a tuple consisting of the median of xs followed by the
    unscaled median absolute deviation of the values in xs that lie
    above the median.
    """
    med = np.median(xs)
    return med, np.median(xs[xs > med] - med)


def insertsizes(bamfiles, chrom, outdir=None, readlength=100, extend=4,
                mads=20, cores=1):
    if outdir is None or not os.path.exists(outdir):
        eprint("outdir should be a valid and existing dir")
        exit(1)
    pool = multiprocessing.Pool(processes=cores)
    launch_ind = partial(insert_size_distro,
                         chrom=chrom,
                         outdir=outdir,
                         readlength=readlength,
                         extend=extend,
                         mads=mads)
    results = pool.map(launch_ind, bamfiles)
    return results


def insert_size_distro(bamfile, chrom, outdir,
                       readlength=100, extend=4, mads=10):
    isizes = []
    rlen = []
    with pysam.AlignmentFile(bamfile) as mybam:
        nb_reads = 0
        for read in mybam.fetch(chrom):
            if read.is_proper_pair:
                isizes.append(np.abs(read.template_length))
                rlen.append(read.infer_query_length())
                nb_reads += 1
            if nb_reads == 100000:
                break

    if len(rlen) == 0:
        raise NoReadsException("No reads found for chromosome " + chrom)

    readlength = max(rlen)

    a = np.array(isizes)
    # remove outliers
    L = a[(a < np.percentile(a, 99)) & (a > np.percentile(a, 1))]
    c = len(L)
    L.sort()
    med, umad = unscaled_upper_mad(L)
    upper_cutoff = med + mads * umad

    # remove extrem insert sizes
    L = L[L < upper_cutoff]

    new_len = len(L)
    removed = c - new_len
    # TODO: sometimes, upper_cutoff is Nan: why? To prevent crash, convert to string
    sys.stderr.write("Removed %d outliers with isize >= %s\n" %
                     (removed, str(upper_cutoff)))

    counts = Counter(list(L-readlength))
    mean = np.mean(L)
    stdev = np.std(L)
    num = len(L)
    sample = fetchId(bamfile)
    histo_file = outdir + "/" + sample + ".histo"
    with open(histo_file, "w") as fout:
        for size, count in counts.items():
            fout.write("%d\t%f\n" % (size, float(count)/num))

    return sample, mean, stdev, histo_file, readlength


def parse_argumanets():
    parser = argparse.ArgumentParser(
        prog="insert_size_distro.py",
        description=" Compute insert size distribution ")
    group_ind = parser.add_mutually_exclusive_group(required=True)
    group_ind.add_argument("-i", "--inFile", dest="inFile",
                           help="A BAM file")
    group_ind.add_argument("-l", "--bamlist", dest="bamlist",
                           help="file with a list of bam files")
    parser.add_argument(
           "-c", "--chrom", required=True,
           help="A comma separated list of chromosomes")
    parser.add_argument(
           "-o", "--outdir", required=True,
           help="Filename for the histogram")
    parser.add_argument(
           "-r", "--readlength", type=int, default=100,
           help="Read length")
    parser.add_argument(
           "-X", dest="X", type=int, default=4,
           help="Number of stdevs from mean to extend")
    parser.add_argument(
           "-m", "--mads", dest="mads", type=int, default=10,
           help="Outlier cutoff in # of median absolute deviations "
                "(unscaled, upper only)")
    parser.add_argument(
           "-t", "--threads",
           default=1, type=int,
           help="number of threads")

    return parser


def main():

    parser = parse_argumanets()
    args = parser.parse_args()

    if args.inFile is not None:
        bamfiles = [args.inFile]
    else:
        bamfiles = get_bamfiles(args.bamlist)

    insertsizes(bamfiles, args.chrom,
                outdir=args.outdir,
                readlength=args.readlength,
                extend=args.X,
                mads=args.mads,
                cores=args.threads)


if __name__ == "__main__":
    sys.exit(main())
